import React from 'react';
import {Route} from "react-router-dom";

import './App.css';
import './css/style.css';

import StartBlockContainer from "./components/startBlock/StartBlockContainer";
import ContactsContainer from "./components/contacts/ContactsContainer";
import HeaderContainer from "./components/header/HeaderContainer";

const App = () => {
    return (
        <div className='app-wrapper'>
            <HeaderContainer/>
            <div className="content">
                <Route exact path='/' render={() => <StartBlockContainer/>}/>
                <Route exact path='/contacts' render={() => <ContactsContainer/>}/>
            </div>
        </div>
    );
};

export default App;
