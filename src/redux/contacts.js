/*const*/
const IS_OPEN_POPUP_CONTACTS = 'IS_OPEN_POPUP_CONTACTS';
const CLOSE_POPUP_CONTACTS = 'CLOSE_POPUP_CONTACTS';
const SET_CONTACTS = 'SET_CONTACTS';
const CLOSE_POPUP_EDIT_CONTACTS = 'CLOSE_POPUP_EDIT_CONTACTS';
const OPEN_POPUP_EDIT_CONTACTS = 'OPEN_POPUP_EDIT_CONTACTS';
const OPEN_POPUP_DELETE_CONTACT = 'OPEN_POPUP_DELETE_CONTACT';
const CLOSE_POPUP_DELETE_CONTACT = 'CLOSE_POPUP_DELETE_CONTACT';

/*initial state*/
let initialState = {
    isAddContact: false,
    isEditContacts: false,
    isDeleteContacts: false,
    contacts: []
};

/*reducer*/
const contacts = (state = initialState, action) => {
    switch (action.type) {
        case IS_OPEN_POPUP_CONTACTS:
            return {
                ...state,
                isAddContact: true
            };
        case CLOSE_POPUP_CONTACTS:
            return {
                ...state,
                isAddContact: false
            };
        case SET_CONTACTS:
            return {
                ...state,
                contacts: action.listContacts
            };
        case CLOSE_POPUP_EDIT_CONTACTS:
            return {
                ...state,
                isEditContacts: false
            };
        case OPEN_POPUP_EDIT_CONTACTS:
            return {
                ...state,
                isEditContacts: true
            };
        case OPEN_POPUP_DELETE_CONTACT:
            return {
                ...state,
                isDeleteContacts: true
            };
        case CLOSE_POPUP_DELETE_CONTACT:
            return {
                ...state,
                isDeleteContacts: false
            };
        default:
            return state;
    }
};

/*action create*/
export const openPopupAddContact = () => ({type: IS_OPEN_POPUP_CONTACTS});
export const closePopupAddContact = () => ({type: CLOSE_POPUP_CONTACTS});
export const openPopupEditContact = () => ({type: OPEN_POPUP_EDIT_CONTACTS});
export const closePopupEditContact = () => ({type: CLOSE_POPUP_EDIT_CONTACTS});
export const openPopupDeleteContact = () => ({type: OPEN_POPUP_DELETE_CONTACT});
export const closePopupDeleteContact = () => ({type: CLOSE_POPUP_DELETE_CONTACT});

export const setContacts = (listContacts) => ({type: SET_CONTACTS, listContacts});

export default contacts;