/*const*/
const OPEN_POPUP_AUTH = 'OPEN_POPUP_AUTH';
const CLOSE_POPUP_AUTH = 'CLOSE_POPUP_AUTH';
const SET_TOKEN = 'SET_TOKEN';
const SET_NAME = 'SET_NAME';

/*initial state*/
let initialState = {
    isOpenPopupAuth: false,
    token: '',
    name_auth_user: ''
};

/*reducer*/
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case OPEN_POPUP_AUTH:
            return {
                ...state,
                isOpenPopupAuth: true
            };
        case CLOSE_POPUP_AUTH:
            return {
                ...state,
                isOpenPopupAuth: false
            };
        case SET_TOKEN:
            return {
                ...state,
                token: action.token
            };
        case SET_NAME:
            return {
                ...state,
                name_auth_user: action.name
            };
        default:
            return state;
    }
};

/*action create*/
export const openPopupAuth = () => ({type: OPEN_POPUP_AUTH});
export const closePopupAuth = () => ({type: CLOSE_POPUP_AUTH});
export const setToken = (token) => ({type: SET_TOKEN, token});
export const setName = (name) => ({type: SET_NAME, name});

export default reducer;