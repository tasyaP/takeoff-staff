import {combineReducers, createStore} from "redux";
import reducer from "./reducer";
import contacts from "./contacts";


let reducers = combineReducers({
    reducer,
    contacts
});

let store = createStore(reducers);

window.store = store;

export default store;