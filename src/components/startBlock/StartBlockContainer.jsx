import {connect} from "react-redux";
import StartBlock from "./StartBlock";


let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        name_auth_user: state.reducer.name_auth_user
    }
};

const startBlockContainer = connect(mapStateToProps, null)(StartBlock);

export default startBlockContainer;