import React, {useState} from 'react';
import {getContacts, updateContact} from "../../ajax/api";

const EditContact = (props) => {
    let currentName = props.currentContact.name;
    let currentPhone = props.currentContact.phone;
    let [changeName, setChangeName] = useState(currentName);
    let [changePhone, setChangePhone] = useState(currentPhone);

    let updateContacts = () => {
        let changeContact = {
            'name': changeName,
            'phone': changePhone
        };
        updateContact(changeContact, props.currentContact.id, props.token)
            .then(() => {
                getContacts(props.token).then(response => {
                    props.setContacts(response.data);
                    props.closePopupEditContact();
                });
            });

    };

    return <div className='overlay'>
        <form className='form'>
            <div className="form__edit">Редактирование контакта</div>
            <div className="close" onClick={() => props.closePopupEditContact()}>x</div>
            <div className='form__field'>
                <label form='name'>Имя</label>
                <input type="text"
                       id="name"
                       value={changeName} onChange={event => setChangeName(event.target.value)}/>
            </div>
            <div className='form__field'>
                <label form='text'>Телефон</label>
                <input type="text"
                       id="phone"
                       value={changePhone} onChange={event => setChangePhone(event.target.value)}/>
            </div>
            <button onClick={event => {
                event.preventDefault();
                updateContacts();
            }}>Обновить
            </button>
        </form>
    </div>
};

export default EditContact;