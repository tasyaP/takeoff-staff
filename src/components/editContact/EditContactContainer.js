import {connect} from "react-redux";
import EditContact from "./EditContact";
import {closePopupEditContact, setContacts} from "../../redux/contacts";
import {updateContact} from "../../ajax/api";

let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        token: state.reducer.token
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        closePopupEditContact: () => {
            dispatch(closePopupEditContact());
        },
        setContacts: (list) => {
            dispatch(setContacts(list));
        }
    }
};

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(EditContact);

export default HeaderContainer;