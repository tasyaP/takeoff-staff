import React from 'react';

const DeleteContact = (props) => {
    return <div className='overlay'>
        <form className='form'>
            <div className="form__title">Удаление контакта</div>
            <div className="close" onClick={() => props.closePopup()}>x</div>
            <div className='form__field'>
                <span>Вы действительно хотите удалить контакт</span>
                <span>{props.currentContact.name}?</span>
            </div>
            <div>
                <button onClick={event => {
                    event.preventDefault();
                    props.deleteContact(props.currentContact.id, props.token).then(() => {
                        props.getContacts(props.token).then(response => {
                            props.setContacts(response.data);
                            props.closePopup();
                        });
                    });
                }}>Да
                </button>
                <button className='cancel' onClick={() => props.closePopup()}>Отмена</button>
            </div>

        </form>
    </div>
};

export default DeleteContact;