import {connect} from "react-redux";
import {closePopupDeleteContact, setContacts} from "../../redux/contacts";
import DeleteContact from "./DeleteContact";
import {deleteContact, getContacts} from "../../ajax/api";

let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        token: state.reducer.token
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        closePopup: () => {
            dispatch(closePopupDeleteContact());
        },
        setContacts: (list) => {
            dispatch(setContacts(list));
        },
        deleteContact: deleteContact,
        getContacts: getContacts
    }
};

const DeleteContactContainer = connect(mapStateToProps, mapDispatchToProps)(DeleteContact);

export default DeleteContactContainer;