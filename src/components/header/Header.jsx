import React from 'react';
import s from './header.module.css';
import NavLink from "react-router-dom/NavLink";
import AuthContainer from "../auth/AuthContainer";

const Header = (props) => {
    return <div>
        <header className={s.header}>
            <div className={s.header__menu}>
                <div className={s.header__link}>
                    <NavLink to="/">Главная</NavLink>
                </div>
                <div className={s.header__link}>
                    <NavLink to="/contacts">Контакты</NavLink>
                </div>
            </div>
            <div className={s.header__link}>
                {
                    props.token
                        ? null
                        : <a href='#' onClick={() => props.openPopupAuth()}>Вход</a>
                }
                {
                    props.isPopupAuth ? <AuthContainer setName={props.setName}/> : null
                }

            </div>
        </header>
    </div>
};

export default Header;