import {connect} from "react-redux";
import Header from "./Header";
import {openPopupAuth, setName} from "../../redux/reducer";


let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        isPopupAuth: state.reducer.isOpenPopupAuth,
        token: state.reducer.token,
        name_auth_user: state.reducer.name_auth_user
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        openPopupAuth: () => {
            dispatch(openPopupAuth());
        },
        setName: (name) => {
            dispatch(setName(name));
        }
    }
}

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);

export default HeaderContainer;