import {connect} from "react-redux";
import AddContact from "./AddContact";

let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        token: state.reducer.token
    }
};

let mapDispatchToProps = (dispatch) => {
    return {}
};

const AddContactContainer = connect(mapStateToProps, mapDispatchToProps)(AddContact);

export default AddContactContainer;