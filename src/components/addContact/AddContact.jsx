import React, {useState} from 'react';
import {addContacts, getContacts} from "../../ajax/api";

const AddContact = (props) => {
    let [name, setName] = useState('');
    let [phone, setPhone] = useState('');

    let updateContacts = (name, phone) => {
        let newContact = {
            "name": name,
            "phone": phone
        };

        addContacts(newContact, props.token).then(() => {
            getContacts(props.token).then(response => {
                props.setContacts(response.data);
                props.closePopup()
            });
        });
    };

    return <div className='overlay'>
        <form className='form'>
            <div className='form__title'> Добавить контакт</div>
            <div className="close" onClick={() => props.closePopup()}>x</div>
            <div className='form__field'>
                <label form='name'>Имя и Фамилия</label>
                <input type="text" id="name" value={name} onChange={event => setName(event.target.value)}/>
            </div>
            <div className='form__field'>
                <label form='text'>телефон</label>
                <input type="text" id="phone" value={phone} onChange={event => setPhone(event.target.value)}/>
            </div>
            <button onClick={(event) => {
                event.preventDefault();
                updateContacts(name, phone)
            }}>Добавить
            </button>
        </form>
    </div>
};

export default AddContact;