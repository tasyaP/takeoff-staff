import {connect} from "react-redux";
import Auth from "./Auth";
import {closePopupAuth, setToken} from "../../redux/reducer";
import {getContacts} from "../../ajax/api";
import {setContacts} from "../../redux/contacts";

let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        token: state.reducer.token
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => {
            dispatch(setToken(token));
        },
        closePopupAuth: () => {
            dispatch(closePopupAuth());
        },
        getContacts: getContacts,
        setContacts: (list) => {
            dispatch(setContacts(list));
        }
    }
};

const AuthContainer = connect(mapStateToProps, mapDispatchToProps)(Auth);

export default AuthContainer;