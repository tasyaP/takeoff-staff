import React, {useState} from 'react';
import {auth, getContacts} from "../../ajax/api";

const Auth = (props) => {
    let [login, setLogin] = useState('');
    let [password, setPassword] = useState('');
    let [error, setError] = useState(false);

    return <div className='overlay'>
        <form className='form'>
            <div className="form__title">Авторизация</div>
            <div className="close" onClick={() => props.closePopupAuth()}>x</div>
            <div className='form__field'>
                <label form='login'>Логин</label>
                <input type="text" id="login" value={login} onChange={event => setLogin(event.target.value)}/>
            </div>
            <div className='form__field'>
                <label form='password'>Пароль</label>
                <input type="password" id="password" value={password}
                       onChange={event => setPassword(event.target.value)}/>
            </div>
            {
                error ? <div className='error'>Логин или пароль введен не верно.</div> : null
            }
            <button onClick={event => {
                event.preventDefault();
                auth(login, password)
                    .then(response => {
                        props.setToken(response.data.accessToken);
                        props.setName(login);
                        getContacts(response.data.accessToken).then((response) => {
                            props.setContacts(response.data);
                        });
                    })
                    .then(() => {
                        props.closePopupAuth();
                    })
                    .catch(() => setError(true));
            }}>Войти
            </button>
        </form>
    </div>
};

export default Auth;