import {connect} from "react-redux";
import Contacts from "./Contacts";
import {
    closePopupAddContact,
    openPopupAddContact, openPopupDeleteContact,
    openPopupEditContact,
    setContacts,
} from "../../redux/contacts";


let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        isAddContact: state.contacts.isAddContact,
        isEditContact: state.contacts.isEditContacts,
        contacts: state.contacts.contacts,
        isDeletePopup: state.contacts.isDeleteContacts,
        token: state.reducer.token
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        openPopup: () => {
            dispatch(openPopupAddContact());
        },
        closePopup: () => {
            dispatch(closePopupAddContact());
        },
        setContacts: (list) => {
            dispatch(setContacts(list));
        },
        openPopupEditContact: () => {
            dispatch(openPopupEditContact());
        },
        openPopupDeleteContact: () => {
            dispatch(openPopupDeleteContact());
        }
    }
};

const ContactsContainer = connect(mapStateToProps, mapDispatchToProps)(Contacts);

export default ContactsContainer;