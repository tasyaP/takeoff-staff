import React, {useEffect, useState} from 'react';
import Contact from "../contact/Contact";
import {getContacts} from "../../ajax/api";
import EditContactContainer from "../editContact/EditContactContainer";
import DeleteContactContainer from "../deleteContact/DeleteContactContainer";
import AddContactContainer from "../addContact/AddContactContainer";

const Contacts = (props) => {
    let [currentContact, setCurrentContact] = useState({});

    let getCurrentContact = (item) => {
        setCurrentContact(item);
    };

    useEffect(() => {
        getContacts(props.token)
            .then(response => {
                props.setContacts(response.data);
            }).catch(() => {
            console.log('Пользователь не авторизован');
        })
    }, []);

    return <>
        {
            props.token ?
                props.contacts.map((item, index) => {
                    return <Contact item={item}
                                    openPopupEdit={props.openPopupEditContact}
                                    currentContact={getCurrentContact}
                                    setContact={props.setContacts}
                                    openDelete={props.openPopupDeleteContact}/>
                })
                :
                <div>Контакты не доступны так как вы не авторизованы.</div>
        }

        {
            props.token
                ? <button onClick={event => {
                    event.preventDefault();
                    props.openPopup()
                }}>Добавить контакт
                </button>
                : null
        }

        {
            props.isAddContact ? <AddContactContainer closePopup={props.closePopup}
                                                      updateContacts={props.updateContacts}
                                                      setContacts={props.setContacts}/> : null
        }
        {
            props.isEditContact ? <EditContactContainer currentContact={currentContact}
                                                        setContact={props.setContacts}/> : null
        }
        {
            props.isDeletePopup ? <DeleteContactContainer currentContact={currentContact}/> : null
        }
    </>
};

export default Contacts;