import React from 'react';
import s from './contact.module.css';

const Contact = (props) => {
    return <div className={s.contacts}>
        <div className={s.contacts__item}>
            <div className={s.contacts__wrap}>
                <div>{props.item.name}</div>
                <a href={'tel:' + props.item.phone} className={s.contacts__phone}>{props.item.phone}</a>
            </div>
            <div className={s.contacts__panel}>
                <div className='icon icon_edit'
                     onClick={
                         () => {
                             props.openPopupEdit();
                             props.currentContact(props.item)
                         }
                     }></div>
                <div className='icon icon_delete' onClick={() => {
                    props.openDelete();
                    props.currentContact(props.item)
                }}>x
                </div>
            </div>
        </div>
    </div>
};

export default Contact;