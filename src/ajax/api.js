import * as axios from "axios";

export const getContacts = (token) => {
    return axios({
        method: 'get',
        url: 'http://localhost:3000/640/contacts',
        accept: 'application/json',
        headers: {
            'User-Agent': 'Console app',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
};

export const addContacts = (newContacts, token) => {
    return axios({
        method: 'post',
        url: 'http://localhost:3000/contacts',
        "Content-Type": 'application/json',
        data: newContacts,
        headers: {
            'User-Agent': 'Console app',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    });
};

export const updateContact = (updateContacts, id, token) => {
    return axios({
        method: 'put',
        url: 'http://localhost:3000/contacts/' + id,
        "Content-Type": 'application/json',
        data: updateContacts,
        headers: {
            'User-Agent': 'Console app',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    });
};

export const deleteContact = (id, token) => {
    return axios({
        method: 'delete',
        url: 'http://localhost:3000/contacts/' + id,
        headers: {
            'User-Agent': 'Console app',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    });
};

export const auth = (login, password) => {
    let data = {
        "email": login,
        "password": password
    };
    return axios({
        method: 'post',
        url: 'http://localhost:3000/login',
        "Content-Type": 'application/json',
        data: data
    });
};